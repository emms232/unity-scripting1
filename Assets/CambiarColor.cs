﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerMove : MonoBehaviour {

    public bool girar;
    public float speed;
    public bool change;
    
	
	// Update is called once per frame
	void Update () {

        
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        transform.rotation = Quaternion.LookRotation(movement);

        if (moveVertical < 0)
        {
            transform.Translate(moveVertical * Vector3.left * Time.deltaTime * speed);
        }
        else if (moveVertical > 0)
        {
            transform.Translate(moveVertical * Vector3.right * Time.deltaTime * speed);
        }

        if (girar == true)
        {
            if (moveHorizontal < 0)
            {
                transform.Rotate(moveHorizontal*Vector3.right*-1 * (speed * Time.deltaTime));
            }
            else if (moveHorizontal > 0)
            {
                transform.Rotate(moveHorizontal*Vector3.right * (speed * Time.deltaTime));
            }
        }
        else if (girar == false)
        {
           

            if (moveHorizontal < 0)
            {
                transform.Translate(moveHorizontal * Vector3.back * Time.deltaTime * speed);
            }
            else if (moveHorizontal > 0)
            {
                transform.Translate(moveHorizontal * Vector3.forward * Time.deltaTime * speed);
            }

        }
    }
}
