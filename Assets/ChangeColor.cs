﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ChangeColor : MonoBehaviour {

    private Renderer colorInicial;
    //public Material coInicial;
    public Color colorRandom;
    private float cInput;
    Renderer rend;

    // Use this for initialization
    void Start () {
        rend = GetComponent<Renderer>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Space))
        {

            colorRandom = new Color32((byte)Random.Range(0,255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255),255);

            rend.material.SetColor("_Color", colorRandom);
        }
        else
        {
            rend.material.SetColor("_Color", Color.white);
        }
    }
}
