﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject objectToSpawn;
    private float time = 0.0f;
    public float interpolationPeriod;
    public GameObject objAux;
    public float radius= 10f;
    float increment;
    [Range(0.5f, 3)]
    public float limit;
    public Color materialcolor;
    Renderer rend;

     void Start()
    {
        rend = objectToSpawn.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        increment += Time.deltaTime;

        if (increment>limit)
        {
            
            increment = 0f;


                Vector3 direct = new Vector3(Random.Range(1, 10), Random.Range(1, 10), Random.Range(1, 10));
                transform.RotateAround(Vector3.zero, Vector3.up, 80 * Time.deltaTime);
                transform.Translate(Vector3.forward * radius * Time.deltaTime);
                objAux = Instantiate(objectToSpawn, transform.position, Quaternion.identity) as GameObject;
                float scale = Random.Range(1, 10);
                objAux.transform.localScale = new Vector3(scale, scale, scale);
                Rigidbody rb = objAux.GetComponent<Rigidbody>();
                rb.drag = Random.Range(0f, 0.1f);
                rb.mass = Random.Range(1, 25);
                rb.AddForce(direct * 100, ForceMode.Impulse);
                
                rend.sharedMaterial.SetColor("_Color", materialcolor);
               
            
        }
    }
}

	

