﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleMovement : MonoBehaviour {

    public float speedH = 2f;
    public float speedV = 2.00f;
    private float ejex = 0;
    private float ejey = 0;
    public float speed = 1f;
    private float hinput, vinput;



    // Update is called once per frame
    void Update() {

        ejex += speedH * Input.GetAxis("Mouse X");
        ejey -= speedH * Input.GetAxis("Mouse Y");
        transform.eulerAngles = new Vector3(ejey, ejex, 0f);
        Rigidbody rb = GetComponent<Rigidbody>();
        hinput = Input.GetAxisRaw("Horizontal");
        vinput = Input.GetAxisRaw("Vertical");

        if (vinput != 0)
        {
            rb.velocity = new Vector3(vinput, 0, 0) * 20f;

        }
        if (hinput != 0)
        {
            rb.velocity = new Vector3(0, 0, hinput) * 20f;

        }

       
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "5Prefab")
        {
            Destroy(gameObject);
        }
    }
}

