﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayShoot : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        RaycastHit objetivo;
        Ray rayo = new Ray(transform.position, transform.forward);

        if (Physics.Raycast(rayo, out objetivo, 1500))
        {
            if (objetivo.collider.gameObject.tag == "5Prefab")
            {
                Destroy(objetivo.collider.gameObject);
            }
        }
	}
}
