﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerInputV2 : MonoBehaviour
{

    public float speed;
    public float runSpeed;
    private float hInput, vInput;
    public bool control;


    // Update is called once per frame
    void Update()
    {
        hInput = Input.GetAxisRaw("Horizontal");
        vInput = Input.GetAxisRaw("Vertical");

        float distance = speed * Time.deltaTime * Input.GetAxis("Horizontal");
        transform.Translate(Vector3.right * distance);

        float step = speed;



        if (Input.GetButton("Run"))
        {
            step = runSpeed;
        }
        if (vInput != 0)
        {
            transform.Translate(vInput * Vector3.forward * Time.deltaTime * step);
        }
        if (hInput != 0)
        {

            if (control == true)
            {
                transform.Translate(hInput * Vector3.right * Time.deltaTime * step);
            }
            else
            {
                transform.Rotate(hInput * Vector3.right * Time.deltaTime * step * 80);
            }
        }
    }
}
